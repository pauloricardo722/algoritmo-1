require_relative 'graph'
require_relative 'graph_search'
require_relative 'node'
require_relative 'build_state'
require_relative 'graph_search'
include GraphSearch

dependencies = [[0,3], [3,5], [5,6], [5,7], [6,7], [4,6], [1,2], [2,6]]

graph = Graph.new
dependencies_order = []

# add nodes and edges to graph
dependencies.each do |dep|
  graph.add_node(Node.new(dep.first)) if graph[dep.first].nil?
  graph.add_node(Node.new(dep.last)) if graph[dep.last].nil?
  graph.add_edge(dep.first, dep.last)
end

graph.nodes.each do |node|
  if node.state == BuildState::BLANK
    return nil if !GraphSearch.dfs(node, dependencies_order)
  end
end

# show result
while !dependencies_order.empty? do
  print "#{dependencies_order.pop} "
end
