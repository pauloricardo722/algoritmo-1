class Node
  attr_reader :name, :successors
  attr_accessor :state

  def initialize(name)
    @name = name
    @successors = []
    @state = BuildState::BLANK
  end

  def add_edge(successor)
    @successors << successor
  end

  def to_s
    "#{@name} -> [#{@successors.map(&:name).join(' ')}]"
  end

end