module GraphSearch
  def dfs(node, order)
    return false if node.state == BuildState::PARTIAL

    if node.state == BuildState::BLANK
      node.state = BuildState::PARTIAL
      node.successors.each do |successor|
        return false if !dfs(successor, order)
      end

      node.state = BuildState::COMPLETE
      order << node.name
    end

    true
  end
end